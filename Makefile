all: train test

train: mnist data/train-images-idx3-ubyte data/train-labels-idx1-ubyte
	ln -sf $< $@

test: mnist data/t10k-images-idx3-ubyte data/t10k-labels-idx1-ubyte
	ln -sf $< $@

mnist: src/main.c 
	$(CC) -o mnist src/main.c -Wall -Wextra -lm


data/%:
	mkdir -p data/
	curl http://yann.lecun.com/exdb/mnist/$$(basename $@).gz | gzip -d > $@

clean:
	rm -f mnist train test
