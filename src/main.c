#include <stdio.h>
#include <stdlib.h>

#include <time.h>
#include <math.h>
#include <string.h>


#ifdef __APPLE__
float __bswap_32(float f)
{
	return __builtin_bswap32(f);
}
#endif

float reluf(float f)
{
	return f > 0 ? f : 0.0001;
}

float sigmoidf(float f)
{
    return 1.f / (1.f + expf(-f));
}

float dreluf(float f)
{
	return f >= 0 ? 1 : 0.0001;
}

float dsigmoidf(float f)
{
	return f * (1 - f);
}

typedef struct {
	int size;
	float *data;
} Vector;

Vector vector_new(int size)
{
	return (Vector){.size = size, .data = malloc(sizeof(float) * size)};
}

void vector_zero(Vector *vec)
{
	for(int i = 0; i < vec->size; ++i) {
		vec->data[i] = 0;
	}
}

Vector vector_from_carr(float *arr, int size)
{
	Vector ret = {.size = size, .data = malloc(sizeof(float) * size)};
	for(int i = 0; i < size; ++i)
		ret.data[i] = arr[i];
	return ret;
}

void vector_free(Vector *vec)
{
	free(vec->data);
	vec->data = NULL;
}

float *vector_get(Vector *vec, int i)
{
	return vec->data + i;
}

void vector_print(const Vector *vec)
{
	printf("[ ");
	for(int i = 0; i < vec->size; ++i) {
		printf("%f " ,vec->data[i]);  
	}
	printf(" ]\n");
}

typedef struct {
	int rows;
	int cols;
	float *data;
} Matrix;

Matrix matrix_new(int rows, int cols)
{
	return (Matrix){.rows = rows, .cols = cols, .data = malloc(sizeof(float) * rows * cols)};
}

void matrix_free(Matrix *m)
{
	free(m->data);
	m->data = NULL;
}

void matrix_zero(Matrix *m)
{
	for(int i = 0; i < (m->rows * m->cols); ++i) m->data[i] = 0;
}

void matrix_rand(Matrix *m)
{
	for(int i = 0; i < (m->rows * m->cols); ++i) m->data[i] =  1 - 2 * (float)rand() / (float)RAND_MAX;
}

void matrix_print(const Matrix *m)
{
	printf("[ ");
	for(int i=0; i<m->rows; ++i) {
		for(int j=0; j<m->cols; ++j) {
			printf("% 6.6f ", m->data[i*m->cols + j]);
		}
		if (i < m->rows - 1) printf("\n  ");
	}
	printf("]");
}

float *matrix_get(const Matrix *m, int row, int col)
{
	return &m->data[row * m->cols + col];
}

typedef struct {
	int *arch;
	int size;
	Vector *layer;
	Matrix *mat;
} Network;

// 2 2 2
Network network_new(int *arch, int size)
{
	Network ret = {.size = size};
	ret.arch = malloc(sizeof(int) * size);
	ret.layer = malloc(sizeof(Vector) * size);
	ret.mat = malloc(sizeof(Matrix) * (size - 1));
	for(int i = 0; i < ret.size; ++i) {
		ret.arch[i] = arch[i];
		ret.layer[i] = vector_new(arch[i] + 1);
		if(i < ret.size - 1) {
			ret.mat[i] = matrix_new(arch[i] + 1, arch[i+1]);
		}

	}
	return ret;
}

void network_rand(Network *net)
{
	for(int i = 0; i < net->size - 1; ++i) {
		matrix_rand(net->mat + i);
	}
}

void network_zero(Network *net)
{
	for(int i = 0; i < net->size - 1; ++i) {
		matrix_zero(net->mat + i);
	}
}

void network_activate(Network *net, Vector *inputs, Vector *outputs)
{
	for(int i = 0;i < inputs->size; ++i)
		//*(vector_get(net->layer, i)) = *(vector_get(inputs, i));
		net->layer[0].data[i] = inputs->data[i];
	net->layer[0].data[net->layer[0].size - 1] = 1;

	for(int i = 0; i < net->size - 1; ++i) {
		for(int j = 0; j < net->layer[i+1].size - 1; ++j) {
			net->layer[i+1].data[j] = 0;
			for(int k = 0; k < net->layer[i].size; ++k) {
				net->layer[i+1].data[j] += net->layer[i].data[k] * *(matrix_get(net->mat + i, k, j));
			}
			net->layer[i+1].data[j] = sigmoidf(net->layer[i+1].data[j]);
			net->layer[i+1].data[net->layer[i+1].size - 1] = 1;
		}

	}
	//vector_print(net->layer + net->size - 1);
	if(!outputs) return;
	for(int i = 0; i < outputs->size; ++i) {
		outputs->data[i] = net->layer[net->size - 1].data[i];
	}

}

typedef struct {
	Vector *input;
	Vector *result;
	int size;
} Batch;


float network_cost(Network *net, Batch *bat) // Vector *inputs, Vector *expected)
{
	const Vector *output = &(net->layer[net->size - 1]);
	//Vector output = vector_new(expected->size);
	float cost = 0;
	for(int j = 0; j < bat->size; ++j) {
		network_activate(net, &bat->input[j], NULL);
//		vector_print(&bat->result[j]); vector_print(output);
		for(int i = 0; i < bat->result[j].size; ++i) {
			const float tmp = output->data[i] - bat->result[j].data[i];
			cost += tmp * tmp;
		}
	}

	return cost / bat->size;

}
void network_grad(Network *net, Batch *bat, Network *grad)
{
	network_zero(grad);
	for(int i = 0; i < bat->size; ++i) {
		network_activate(net, bat->input + i, NULL);
		//vector_print(&bat->result[i]);
		//vector_print(output);
		for(int j = 0; j < grad->size; ++j) {
			vector_zero(grad->layer + j);
			grad->layer[j].data[grad->layer[j].size - 1] = 1;
		}
		for(int j = 0; j < bat->result[i].size; ++j) {
			grad->layer[grad->size - 1].data[j] = 2 * (net->layer[net->size - 1].data[j] - bat->result[i].data[j]);
		}
		for(int layer = net->size - 1; layer > 0; --layer) {
			for(int j = 0; j < net->layer[layer].size - 1; ++j) {
				float da = grad->layer[layer].data[j];
				float qa = dsigmoidf(net->layer[layer].data[j]);
				for(int k = 0; k < net->layer[layer - 1].size; ++k) {
					float pa = net->layer[layer - 1].data[k];
					float w = *(matrix_get(&net->mat[layer - 1], k, j));
					*(matrix_get(&grad->mat[layer - 1], k, j)) += da * qa * pa;
					grad->layer[layer - 1].data[k] += da * qa * w;
				}
				grad->layer[layer - 1].data[grad->layer[layer - 1].size - 1] = 1;

			}
		}

		
	}
	for(int k = 0; k < net->size - 1; ++k) {
		for(int i = 0; i < (grad->mat[k].rows * grad->mat[k].cols); ++i) grad->mat[k].data[i] /= bat->size;
	}
}

void network_learn(Network *net, Network *grad, float rate)
{
	for(int k = 0; k < net->size - 1; ++k) {
		for(int i = 0; i < (net->mat[k].rows * net->mat[k].cols); ++i) net->mat[k].data[i] -= rate * grad->mat[k].data[i];
	}


}

void network_print(const Network *net)
{	
	
	for(int i = 0; i < net->size; ++i) 
		printf("%d, ", net->arch[i]);
	printf("\n");
	for(int i = 0; i < net->size - 1; ++i) {
		matrix_print(net->mat + i);
		printf("\n");
	}

}

void network_save(Network *net, const char *output)
{
	FILE *fp = fopen(output, "wb");
	fwrite(&net->size, sizeof(int), 1, fp);
	fwrite(net->arch, sizeof(int), net->size, fp);
	for(int i = 0; i < net->size - 1; ++i)
	{
		fwrite(net->mat[i].data, sizeof(float), net->mat[i].cols * net->mat[i].rows, fp);
	}
	fclose(fp);
}

Network network_load(const char *input)
{
	Network ret;
	FILE *fp = fopen(input, "rb");
	fread(&ret.size, sizeof(int), 1, fp);
	printf("%d\n", ret.size);
	ret.arch = malloc(sizeof(int) * ret.size);
	fread(ret.arch, sizeof(int), ret.size, fp);
	printf("%d\n", ret.arch[0]);

	ret.layer = malloc(sizeof(Vector) * ret.size);
	ret.mat = malloc(sizeof(Matrix) * (ret.size - 1));

	for(int i = 0; i < ret.size; ++i) {
		ret.layer[i] = vector_new(ret.arch[i] + 1);
		if(i < ret.size - 1) {
			ret.mat[i] = matrix_new(ret.arch[i] + 1, ret.arch[i+1]);
			fread(ret.mat[i].data, sizeof(float), ret.mat[i].cols * ret.mat[i].rows, fp);
		}

	}

	return ret;

}

Batch *batch_slice(Batch *bat, int size)
{
	const int slice_size = bat->size / size;
	Batch *ret = malloc(sizeof(Batch) * slice_size);
	for(int i = 0; i < size; ++i) {
		ret[i].size = slice_size;
		ret[i].input = bat->input + i * slice_size;
		ret[i].result = bat->result + i * slice_size;
	}
	return ret;
}

Batch xor_batch(void)
{
	const int size = 4;
	Batch ret = {  .input = malloc(sizeof(Vector) * size),
		.result = malloc(sizeof(Vector) * size),
		.size = size};
	float data[] = {0, 0, 1, 0,
			1, 0, 0, 1,
			0, 1, 0, 1,
			1, 1, 1, 0};
	ret.input[0] = vector_from_carr(data, 2);
	ret.input[1] = vector_from_carr(data + 4, 2);
	ret.input[2] = vector_from_carr(data + 8, 2);
	ret.input[3] = vector_from_carr(data + 12, 2);
	
	ret.result[0] = vector_from_carr(data + 2, 2);
	ret.result[1] = vector_from_carr(data + 6, 2);
	ret.result[2] = vector_from_carr(data + 10, 2);
	ret.result[3] = vector_from_carr(data + 14, 2);
	return ret;
}

Batch mnist_batch(const char *data, const char *label)
{
	Batch ret = {0};
	FILE *fp = fopen(data, "rb");
	int magic = 0;
	fread(&magic, sizeof(int), 1, fp);
	magic = __bswap_32(magic);
	printf("%x\n", magic);
	int count = 0;
	fread(&count, sizeof(int), 1, fp);
	count = __bswap_32(count);
	printf("%d\n", count);

	int rows = 0, cols = 0;

	fread(&rows, sizeof(int), 1, fp);
	rows = __bswap_32(rows);
	fread(&cols, sizeof(int), 1, fp);
	cols = __bswap_32(cols);

	printf("%d, %d\n", rows, cols);

	ret.size = count;
	ret.input = malloc(sizeof(Vector) * count);
	ret.result = malloc(sizeof(Vector) * count);

	unsigned char *buff = malloc(sizeof(unsigned char) * cols * rows);
	for(int i = 0; i < ret.size; ++i) {
		fread(buff, sizeof(unsigned char), rows * cols, fp);
		ret.input[i] = vector_new(rows * cols);
		for(int j = 0; j < ret.input[i].size; ++j) {
			ret.input[i].data[j] = (float) buff[j] / 255.0f;
		}
	}

	/*for(int i = 0; i < rows; ++i) {
		for(int j = 0; j < cols; ++j) {
			printf("%.3f ", ret.input[ret.size - 1].data[rows * i + j]);
		}
		printf("\n");
	}*/
	fclose(fp);
	fp = fopen(label, "rb");
	magic = 0;
	fread(&magic, sizeof(int), 1, fp);
	magic = __bswap_32(magic);
	printf("%x\n", magic);
	count = 0;
	fread(&count, sizeof(int), 1, fp);
	count = __bswap_32(count);
	printf("%d\n", count);

	for(int i = 0; i < ret.size; ++i) {
		unsigned char sbuff = 0;
		fread(&sbuff, sizeof(unsigned char), 1, fp);
		ret.result[i] = vector_new(10);
		for(unsigned char j = 0; j < 10; ++j) {
			ret.result[i].data[j] = sbuff == j ? 1.0f : 0.0f;
		}
	}
	//vector_print(&ret.result[ret.size - 1]);

	free(buff);
	return ret;
}

void batch_print(const Batch *bat)
{
	for(int i = 0; i < bat->size; ++i) {
		printf("id: %d\tinput: [ ", i);
		for(int j = 0; j < bat->input[i].size; ++j) {
			printf("%f ", bat->input[i].data[j]);
		}
		printf("]\toutput: [ ");
		for(int j = 0; j < bat->result[i].size; ++j) {
			printf("%f ", bat->result[i].data[j]);
		}
		printf("]\n");
	}
}

int train(int argc, char *argv[])
{

	clock_t start, end;
	Network n1 = network_new((int[]){784, 16, 16, 10}, 4);
	network_rand(&n1);
	Network g = network_new((int[]){784, 16, 16, 10}, 4);
	network_print(&n1);



	//Batch b1 = xor_batch();
	//batch_print(&b1);
	Batch b1 = mnist_batch("data/train-images-idx3-ubyte", "data/train-labels-idx1-ubyte");
	const int bs_size = 60;
	Batch *bs = batch_slice(&b1, bs_size);


	for(int i = 0; i < 5000; ++i) {
		const float cost = network_cost(&n1, bs + (i % bs_size));
		printf("epoch = %d, cost = %f", i, cost);
		fflush(stdout);
		start = clock();
		network_grad(&n1, bs + (i % bs_size), &g);
		network_learn(&n1, &g, 0.1f);
		end = clock();
		const double elps = (double)(end - start) / CLOCKS_PER_SEC;
		printf(", done in %f.\n", elps);
	}

	network_save(&n1, "mnist.model");

	return 0;
}

int test(int argc, char *argv[])
{
	Network n1 = network_load("mnist.model");
	Batch b1 = mnist_batch("data/t10k-images-idx3-ubyte", "data/t10k-labels-idx1-ubyte");

	int success_count = 0;

	Vector output = vector_new(10);
	for(int i = 0; i < b1.size; ++i) {
		network_activate(&n1, &b1.input[i], &output);
		float result_max = 0.0f;
		int result_index = -1;
		for(int j = 0; j < b1.result[i].size; ++j) {
			if(result_max < b1.result[i].data[j]) {
				result_index = j;
				result_max = b1.result[i].data[j];
			}
		}
		float output_max = 0.0f;
		int output_index = -1;
		for(int j = 0; j < output.size; ++j) {
			if(output_max < output.data[j]) {
				output_index = j;
				output_max = output.data[j];
			}
		}
		//printf("%d: output is %d, result is %d\n", i, output_index, result_index);
		//printf("%d: ", i);
		//vector_print(&output);
		if(output_index == result_index) success_count++;

	}

	const double success_precent = (double) success_count / (double) b1.size;
	printf("Test is complete with %f%% success rate\n", success_precent * 100);

	return 0;
}

int main(int argc, char *argv[])
{
	srand(time(NULL));

	if(strcmp(argv[0], "./train") == 0) return train(argc, argv);
	if(strcmp(argv[0], "./test") == 0) return test(argc, argv);
	fprintf(stderr, "Please use one of the utilities to run the program\n");


	return 1;
}
